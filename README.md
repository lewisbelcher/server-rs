Server
======

A real simple HTTP server built using `warp`.


Certs
-----

Using a self-signed certificate created using `openssl`:
```
openssl req -newkey rsa:4096 -nodes -sha512 -x509 -days 999 -out certs/cert.pem -keyout certs/key.rsa
```

Make sure to use the `https` protocol and `-k` option in `curl` to switch off
actually being safe :) (e.g. `curl -k https://127.0.0.1:3030/hello/you`).
