# NB it seems futures-macro doesn't support musl, so we cannot use alpine..
FROM rust:slim-buster as builder
WORKDIR /usr/src/app
COPY Cargo* ./
COPY src src
RUN cargo install --path .

FROM debian:buster-slim
COPY --from=builder /usr/local/cargo/bin/server-rs /usr/local/bin/
WORKDIR /srv
ENTRYPOINT ["server-rs"]
