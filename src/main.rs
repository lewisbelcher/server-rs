#[macro_use]
extern crate log;
mod endpoints;
use warp::Filter;

#[tokio::main]
async fn main() {
	pretty_env_logger::init();

	let app = endpoints::hello::all()
		.or(endpoints::item::all())
		.with(warp::log("server-rs"));

	debug!("Starting server...");
	warp::serve(app)
		.tls()
		.cert_path("certs/cert.pem")
		.key_path("certs/key.rsa")
		.run(([0, 0, 0, 0], 3030))
		.await;
}
