use super::handlers::item;
use warp::{Filter, Rejection, Reply};

// It'll be real swell when rust stabilizes type aliases for impl.
static PATH: &'static str = "item";

pub fn all() -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
	get().or(post())
}

pub fn get() -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
	warp::path(PATH)
		.and(warp::get())
		.and(warp::path::param::<String>())
		.and_then(item::get)
}

pub fn post() -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
	warp::path(PATH)
		.and(warp::post())
		.and(warp::path::param::<String>())
		.and_then(item::post)
}
