use super::handlers::hello;
use warp::{Filter, Rejection, Reply};

pub fn all() -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
	get()
}

pub fn get() -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
	warp::path!("hello" / String)
		.and(warp::get())
		.and_then(hello::get)
}
