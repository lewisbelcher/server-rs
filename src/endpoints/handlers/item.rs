use serde::{Deserialize, Serialize};
use std::convert::Infallible;

#[derive(Deserialize, Serialize)]
struct Item {
	text: String,
	value: u32,
}

/// Get an item from the database.
pub async fn get(name: String) -> Result<impl warp::Reply, Infallible> {
	let item = Item { text: "My text".to_string(), value: 34 };
	Ok(warp::reply::json(&item))
}

/// Post an item to the database.
pub async fn post(name: String) -> Result<impl warp::Reply, Infallible> {
	// TODO: store content
	Ok(format!("Added {}!", name))
}
