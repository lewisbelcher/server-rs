use std::convert::Infallible;

pub async fn get(name: String) -> Result<impl warp::Reply, Infallible> {
	Ok(format!("Hello, {}!", name))
}
